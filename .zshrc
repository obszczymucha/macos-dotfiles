# shellcheck disable=SC2148
export OHMYZSH_DIR=$HOME/.oh-my-zsh
export DOTFILES_DIR="$HOME/.dotfiles"
export ZSHRC_DIR="$DOTFILES_DIR/zshrc"
export SCRIPTS_DIR="$DOTFILES_DIR/Scripts"
export EDITOR=nvim
export NVM_LAZY_LOAD=true
export NVM_COMPLETION=true
export NVIM_DEPENDENCIES_DIR="$HOME/.config/nvim/dependencies"
export ZLE_REMOVE_SUFFIX_CHARS=""
export PATH=$PATH:$DOTFILES_DIR/Scripts

# shellcheck disable=SC2034
ZSH_THEME="alien"

setopt correct

# shellcheck disable=SC2034
plugins=(
  zsh-nvm
  evalcache
  git
  autojump
  zsh-syntax-highlighting
  aws
)

# shellcheck disable=SC2086,1091
{
  source $OHMYZSH_DIR/oh-my-zsh.sh
  source $ZSHRC_DIR/remap.zsh
  source $ZSHRC_DIR/help.zsh
  source $ZSHRC_DIR/general-purpose.zsh
  source $ZSHRC_DIR/git.zsh
  source $ZSHRC_DIR/haskell.zsh
  source $ZSHRC_DIR/java.zsh
  source $ZSHRC_DIR/navigation.zsh
  source $ZSHRC_DIR/work.zsh
  source $ZSHRC_DIR/work-private.zsh
  source $ZSHRC_DIR/tmux.zsh
  source $ZSHRC_DIR/pyenv.zsh
}
