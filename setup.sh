#!/usr/bin/env bash
DOTFILES_DIR="$HOME/.dotfiles"
PROJECTS_DIR="$HOME/projects"
TIMESTAMP=$(date +"%Y%m%dT%H%M%S")

function link_dotfiles() {
  echo "Linking dotfiles..."

  local current_dir
  current_dir=$(pwd -P)

  ln -sfn "$current_dir" "$DOTFILES_DIR"
}

function install_brew() {
  if command -v brew &>/dev/null; then
    echo "Found Homebrew."
    return
  fi

  echo "Installing Homebrew..."

  /bin/bash -c "$(curl -fssl https://raw.githubusercontent.com/homebrew/install/head/install.sh)"
  # shellcheck disable=SC2016
  (echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> "$HOME/.zprofile"
  eval "$(/opt/homebrew/bin/brew shellenv)"
}

function install_tools_via_brew() {
  echo "Installing basic tools..."
  brew install grep alacritty barrier tmux fd ripgrep gpg autojump typescript go amethyst neovim wget shellcheck autoconf pkg-config black isort fzf
  brew install koekeishiya/formulae/skhd
}

function backup() {
  local filename="$1"

  if [[ -f "$filename" ]]; then
    echo "Backing up to $filename to $filename.$TIMESTAMP.old"
    cp "$HOME/.zshrc" "$HOME/.zshrc.$TIMESTAMP.old"
  fi
}

function link_zshrc {
  echo "Linking .zshrc..."
  local filename="$HOME/.zshrc"

  backup "$filename"
  ln -sfn "$DOTFILES_DIR/.zshrc" "$filename"
}

function link_tmux_config {
  echo "Linking .tmux.conf..."
  local filename="$HOME/.tmux.conf"

  backup "$filename"
  ln -sfn "$DOTFILES_DIR/.tmux.conf" "$filename"
}

function git_clone_or_update() {
  local name="$1"
  local source_url="$2"
  local target_dir="$3"

  if [[ -d "$target_dir" ]]; then
    echo "Found $name. Updating..."
    git -C "$target_dir" pull --rebase
  else
    echo "Installing $name..."
    git clone "$source_url" "$repo_dir" 2> /dev/null
  fi

  if [[ $# == 4 ]]; then
    echo "Linking $name..."
    ln -sfn "$target_dir" "$4"
  fi
}

function link_alacritty_config() {
  echo "Setting up alacritty configuration..."
  local dir="$HOME/.config/alacritty"
  local filename="$dir/alacritty.yml"
  mkdir -p "$dir"

  backup "$filename"
  ln -sfn "$DOTFILES_DIR/.config/alacritty/alacritty.yml" "$filename"
}

function link_mac_keybindings() {
  echo "Linking Mac's keybindings..."
  local lib_dir="$HOME/Library/KeyBindings"
  mkdir -p "$lib_dir"
  cp "$lib_dir/DefaultKeyBinding.dict" "$DOTFILES_DIR/DefaultKeyBinding.dict.$TIMESTAMP.old"
  ln -sfn "$DOTFILES_DIR/DefaultKeyBinding.dict" "$lib_dir/DefaultKeyBinding.dict"
}

function install_fonts() {
  echo "Installing fonts..."
  brew tap homebrew/cask-fonts && brew install font-roboto-mono-nerd-font
}

function install_tpm() {
  git_clone_or_update "tmux plugin manager" \
    "$HOME/.tmux/plugins/tpm" \
    "https://github.com/tmux-plugins/tpm"

  echo "Press <prefix>+R to reload tmux, then <prefix>+I to install plugins."
}

function fix_local_dir_ownership() {
  local local_dir="$HOME/.local"

  local user
  user=$(stat -f '%Su' "$HOME")

  local group
  group=$(stat -f '%Sg' "$HOME")

  local local_user
  local_user=$(stat -f '%Su' "$local_dir")

  local local_group
  local_group=$(stat -f '%Sg' "$local_dir")

  if [[ "$local_user" != "$user" && "$local_group" != "$group" ]]; then
    echo "Fixing $HOME/.local directory ownership..."
    sudo chown -R "${user}:${group}" "$local_dir"
  fi
}

function setup_nvim_config() {
  echo "Setting up neovim config..."
  local repo_dir="$PROJECTS_DIR/system/nvim-config"

  git_clone_or_update "nvim-config" "git@codeberg.org:obszczymucha/nvim-config.git" "$repo_dir"

  ln -sfn "${repo_dir}/config" "$HOME/.config/nvim"
}

function install_chrome_shortcuts_extension() {
  git_clone_or_update "chrome-shortcuts-extension" \
    "git@codeberg.org:obszczymucha/chrome-shortcuts-extension.git" \
    "$PROJECTS_DIR/system/chrome-shortcuts-extension"
}

function setup_skhd() {
  echo "Linking skhd config..."
  ln -sfn "$DOTFILES_DIR/.skhdrc" "$HOME/.skhdrc"
}

link_dotfiles
install_brew
install_tools_via_brew
link_zshrc
link_tmux_config
link_alacritty_config
link_mac_keybindings
install_fonts
install_tpm
fix_local_dir_ownership
setup_nvim_config
install_chrome_shortcuts_extension
install_openjdk 11
install_openjdk 17
install_jvm_tools
setup_skhd

