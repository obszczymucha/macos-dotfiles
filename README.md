# MacOS .dotfiles for productivity

## Disable desktop icons

```bash
defaults write com.apple.finder CreateDesktop -bool false
```

## Shift+Alt+Enter shortcut for Alacritty
Mac sucks and it's keybinding UI doesn't allow mapping to Enter.  
This is the way to do it.

1. Use `Automator` to create a `Quick Action` to `Launch Application` for
   Alacritty and save it as `Launch Alacritty`.

2. Run:
   ```bash
   defaults read pbs | grep 'Launch Alacritty' -C5
   ```

   This will show the current binding for `Launch Alacritty` service.

3. Run:
   ```bash
   /usr/libexec/PlistBuddy -c "set :NSServicesStatus:'(null) - Launch Alacritty - runWorkflowAsService':key_equivalent @$↩" ~/Library/Preferences/pbs.plist
   ```

   This will set the keybinding.

4. Run point 2. again to confirm.
5. Restart the system.

