# shellcheck disable=SC2148
export PROJECTS_DIR="$HOME/projects"
alias p="cd $PROJECTS_DIR"
alias pylogin="bash <(aws s3 cp s3://aips-builds/authenticate.sh -) python"

# Barrier
alias bar="killall barrierc; /Applications/Barrier.app/Contents/MacOS/barrierc --name work --disable-crypto barrier-host:9696"

function catjq() {
  cat $1 | jq
}
