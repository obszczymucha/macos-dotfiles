# shellcheck disable=SC2148
alias glol="git log --pretty=format:'%C(yellow)%h|%Cred%ad|%Cblue%an|%Cgreen%d %Creset%s' --date=short | column -ts'|' | less -r"
alias gh="git rev-parse HEAD"
alias ghash="git rev-parse"
alias grbom='git rebase origin/master'
alias gitsub="git submodule update --init --recursive"
alias grbfh='git rebase FETCH_HEAD'
alias gp > /dev/null && unalias gp

function gp() {
  if [[ $# > 1 ]]; then
    local cmd="git push $@"
    echo "Running: $cmd"
    eval "$cmd"
    return
  fi

  local branch="$1"

  if [[ -z "$branch" ]]; then
    branch=$(git branch --show-current)
  fi

  local cmd="git push origin $branch"
  echo "Running: $cmd"
  eval "$cmd"
}

function home_to_dotfiles() {
  readonly current_dir=$(pwd)

  if [[ $current_dir == $HOME  ]]; then
    cd "$DOTFILES_DIR"
    command $@
  else
    command $@
  fi
}

unalias gst
function gst() {
  home_to_dotfiles git status
}

unalias gpr
function gpr() {
  readonly current_dir=$(pwd)

  if [[ $current_dir == $HOME ]]; then
    cd "$DOTFILES_DIR"
  fi

  if [[ ! $(is_git_worktree) && ! $(is_git_bare_repo_worktree) ]]; then
    local branch=$(git branch --show-current)
    git pull origin "$branch" --rebase
  else
    git pull --rebase
  fi
}

unalias gco
function gco() {
  git checkout "$@"
}

unalias gd
function gd() {
  home_to_dotfiles git --no-pager diff
}

function gdp() {
  home_to_dotfiles git diff
}

function gdc() {
  home_to_dotfiles git --no-paged diff --cached
}

function gdcp() {
  home_to_dotfiles git diff --cached
}

unalias gcl
function gcl() {
  git clone "$@"
}

function is_git_worktree {
  if [[ $# == 0 && "$(git rev-parse --is-inside-work-tree 2>/dev/null)" == "true" ]]; then
    return 0
  elif [[ $# == 1 && "$(git -C $1 rev-parse --is-inside-work-tree 2>/dev/null)" == "true" ]]; then
    return 0
  else
    return 1
  fi
}

function is_git_bare_repo_worktree {
  if [[ $# == 0 && "$(git config --get core.bare)" == "true" ]]; then
    return 0
  elif [[ $# == 1 && "$(git -C $1 config --get core.bare)" == "true" ]]; then
    return 0
  else
    return 1
  fi
}

function is_bare_git_repository {
  if [[ $# == 0 && "$(git rev-parse --is-bare-repository 2>/dev/null)" == "true" ]]; then
    return 0
  elif [[ $# == 1 && "$(git -C $1 rev-parse --is-bare-repository 2>/dev/null)" == "true" ]]; then
    return 0
  else
    return 1
  fi
}

