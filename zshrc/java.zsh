# shellcheck disable=SC2148
# shellcheck disable=SC2155
{
  local jdtls_dir="/opt/homebrew/opt/jdtls"
  export JAVA_HOME=$(/usr/libexec/java_home -v 17)
  export JAVA17_BINARY=$(/usr/libexec/java_home -v 17)/bin/java
  export JDTLS_PLUGIN=$(ls $jdtls_dir/libexec/plugins/org.eclipse.equinox.launcher_*)
  export JDTLS_CONFIG="$jdtls_dir/libexec/config_mac"
  export JAVA_DEBUG_PLUGIN_DIR="/Users/mkarbowy/projects/java-debug"
  export VSCODE_JAVA_TEST_EXTENSION_DIR="/users/mkarbowy/projects/vscode-java-test"
  export PATH=$PATH:$JAVA_HOME/bin
}

