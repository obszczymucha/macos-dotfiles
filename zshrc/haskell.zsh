# shellcheck disable=SC2148
alias hls=haskell-language-server-wrapper
alias htags='hasktags -c -a -f .tags .'
export PATH="$PATH:$HOME/.local/bin"
[ -f "$HOME/.ghcup/env" ] && source "$HOME/.ghcup/env" # ghcup-env

