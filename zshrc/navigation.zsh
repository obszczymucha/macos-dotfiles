# shellcheck disable=SC2148
source $ZSHRC_DIR/autols.zsh

DIRJUMP_DIRECTORY=~/.dirjump
mkdir -p "$DIRJUMP_DIRECTORY"
MAIN_DIRECTORY_FILE="$DIRJUMP_DIRECTORY"/main
LAST_DIRECTORY_FILE="$DIRJUMP_DIRECTORY"/last
SECONDARY_DIRECTORY_FILE="$DIRJUMP_DIRECTORY"/secondary

function _m() {
  local FILE="$1"

  function set_dir() {
    echo "$(pwd)" > "$FILE"
    echo $(pwd)
  }

  if [[ $2 == "set" || ! -f "$FILE" ]]; then
    set_dir
    return
  fi

  local DIRECTORY=$(cat "$FILE")

  if [[ $(pwd) == "$DIRECTORY" ]]; then
    if [[ $# > 2 ]]; then
      cd "$3"
    fi

    return
  fi

  echo $(pwd) > "$LAST_DIRECTORY_FILE"
  cd "$DIRECTORY"

  if [[ $# > 2 ]]; then
    cd "$3"
  fi
}

function m() {
  _m "$MAIN_DIRECTORY_FILE"
}

function ms() {
  _m "$MAIN_DIRECTORY_FILE" set
}

function mm() {
  _m "$SECONDARY_DIRECTORY_FILE"
}

function mms() {
  _m "$SECONDARY_DIRECTORY_FILE" set
}

function mb() {
  if [[ -f "$LAST_DIRECTORY_FILE" ]]; then
    local TMP="$(pwd)"
    local LAST_DIRECTORY=$(cat "$LAST_DIRECTORY_FILE")
    cd "$LAST_DIRECTORY"
    echo "$TMP" > "$LAST_DIRECTORY_FILE"
  fi
}

function b() {
  if [[ ${#dirstack[@]} == 0 ]]; then
    mb
  else
    cd -
    echo "$(pwd)" > "$LAST_DIRECTORY_FILE"
  fi
}

DIRSTACKSIZE=8
setopt autopushd pushdminus pushdsilent pushdtohome
alias dh='dirs -v'

function cd() {
  ERROR=$(builtin cd "$@" 2>&1 > /dev/null)
  local RESULT=$?

  if [[ ! $RESULT == "0" ]]; then
    echo $ERROR | sed -e "s/^cd\:cd\:[^:]\+:/cd\:/g"
    return $RESULT
  else
    builtin cd "$@"
    autols
    echo "$(pwd)" > "$LAST_DIRECTORY_FILE"
  fi
}

