# shellcheck disable=SC2148
alias t=tmux
alias ta="t a"
alias tn="t new-session -d"
alias n="t rename-window"
alias N="t rename-session"

function immutable_tmux() {
  if [[ $# != 3 ]]; then
    echo "immutable_tmux: 3 parameters required"
    return 1
  fi

  local session_name=$1
  local window_name=$2
  local cmd=$3


  if [[ -z $TMUX ]]; then
    if tmux attach-session -t "$session_name:$window_name" 2> /dev/null; then
      return
    fi
  else
    if tmux switch-client -t "$session_name:$window_name" 2> /dev/null; then
      return
    fi
  fi
  
  if [[ -z $TMUX ]]; then
    tmux new-session -s "$session_name" -n "$window_name" "$cmd"
  else
    if ! tmux has-session -t "$session_name" 2> /dev/null; then
      tmux new-session -s "$session_name" -n "$window_name" -d "$cmd"
      tmux switch-client -t "$session_name:$window_name"
    else
      tmux new-window -b -t "$session_name:" -n "$window_name" "$cmd"
      tmux switch-client -t "$session_name:$window_name"
    fi
  fi
}

function vc() {
  immutable_tmux nvim-config code 'nvim +"cd ~/.config/nvim" +HarpoonFirst'
}

function vcc() {
  tm nvim-config "$HOME/.config/nvim" "nvim +HarpoonFirst"
}

function rc() {
  immutable_tmux zshrc zshrc 'nvim ~/.zshrc +"cd $ZSHRC_DIR" +HarpoonFirst'
}

function tm() {
  if [[ $# -eq 0 ]]; then
    echo "Usage: $0 <tmux_session_name>"
    return 1
  fi

  local session_name="$1"
  tmux has-session -t $session_name > /dev/null 2>&1

  if [ $? -eq 0 ]; then
    tmux switch-client -t $session_name
  else
    local window_count
    window_count=$(tmux list-windows | wc -l)

    local start_dir="$PWD"

    if [[ $# -gt 1 ]]; then
      start_dir="$2"
    fi

    if [[ $window_count -eq 1 ]]; then
      tmux rename-session "$session_name"
      tmux rename-window "code"
      tmux new-window -d -n "shell"
    else
      tmux new-session -d -s "$session_name" -n code "cd '$start_dir'; STARTUP_CMD='$3' $SHELL"
      tmux new-window -t "$session_name:2" -n shell "cd '$start_dir'; $SHELL"
      tmux select-window -t $session_name:1
      tmux switch-client -t $session_name
    fi
  fi
}

if [[ -z $TMUX && -z $NO_TMUX ]]; then
  if [[ -z $STARTUP_CMD ]]; then
    t
  else
    tmux -c "STARTUP_CMD=\"$STARTUP_CMD\" zsh"
  fi
fi

if [[ -n $TMUX && -n $STARTUP_CMD ]]; then
  eval "$STARTUP_CMD"
fi
