# shellcheck disable=SC2148
alias browse="tree -d -L 2 -C"
alias c="clear"
alias cvc='cd ~/.config/nvim'
alias fuck="clear;HISTSIZE=0;HISTSIZE=50"
alias pwdp='pwd -P'
alias src="NO_TMUX=1 source  ~/.zshrc"
alias vim=nvim
alias x=exit
alias gpgtty="export GPG_TTY=$(tty)"
alias java_home="/usr/libexec/java_home -v"

alias v_="nvim -u NONE"
alias l > /dev/null && unalias l
alias l="gls -lh --group-directories-first --color"
alias ll > /dev/null && unalias ll
alias ll="gls -lah --group-directories-first --color"
alias la > /dev/null && unalias la
alias la=ll
alias cl="c && l"
alias cll="c && ll"

function tl() {
  local level=2

  if [[ $# > 0 ]]; then
    level="$1"
  fi

  tree --dirsfirst -L $level
}

function tla() {
  local level=2

  if [[ $# > 0 ]]; then
    level="$1"
  fi

  tree -a --dirsfirst -L $level
}

function renvim() {
  local nvim_share_dir="$HOME/.local/share/nvim"
  mv "${nvim_share_dir}/harpoon.json" /tmp/harpoon.json
  rm -rf "$nvim_share_dir"
  mkdir -p "$nvim_share_dir"
  mv /tmp/harpoon.json "${nvim_share_dir}/harpoon.json"
}

function v() {
  if [[ $# -eq 0 ]]; then
    nvim +"lua require( \"harpoon.ui\" ).nav_file( 1 )"
  else
    nvim "$@"
  fi
}

function aa() {
  if [[ $# -lt 2 ]]; then
    echo "Adds alias to .zshrc."
    echo "Usage: aa <alias_name> <alias_body>"
    return 1
  fi

  alias $1 > /dev/null

  if [[ $? -eq 0 ]]; then
    echo "Alias $1 is already defined!"
    return 2
  fi

  local cmd="alias $1='${@:2}'"
  echo $cmd >> "$DOTFILES_DIR"/.zshrc
  eval $cmd
  echo "Alias $1 added."
}

function ra() {
  if [[ $# -eq 0 ]]; then
    echo "Removes alias from .zshrc."
    echo "Usage: ra <alias_name>"
    return 1
  fi

  local sed_expression="/^alias $1=/d"
  sed -i $sed_expression "$DOTFILES_DIR"/.zshrc

  alias $1 > /dev/null

  if [[ $? -eq 0 ]]; then
    unalias $1
    echo "Alias $1 removed."
  fi
}

function fn() {
  if [[ $# == 0 ]]; then return; fi
  local funcname="$1"

  local builtin_type
  builtin_type=$(builtin type "$funcname")

  local sed_expression="s/^${funcname} is a shell function from (.+)\$/\\1/g"
  local full_path
  full_path=$(echo "$funcname" | grep "is a shell function from" | sed -E "$sed_expression")

  local line_number

  if [[ -z "$full_path" ]]; then
    if ! echo "$builtin_type" | grep "is an alias for" > /dev/null; then
      echo "$builtin_type"
      return
    fi

    read -r full_path line_number <<< $(ggrep -nr "alias ${funcname}" "$DOTFILES_DIR" | head -n 1 | sed -E 's/^(.+):([0-9]+):.*$/\1 \2/g')
  else
    line_number=$(grep -n "${funcname}()" "$full_path" | sed -E 's/^([0-9]+):.*$/\1/g')
  fi

  nvim +"$line_number" +"normal! zz" "$full_path"
}

function fn() {
  if [[ $# == 0 ]]; then return; fi
  local funcname="$1"
  local search_dir="$2"
  local full_path
  local line_number

  if [[ -z "$search_dir" ]]; then
    local builtin_type
    search_dir="$DOTFILES_DIR"
    builtin_type=$(builtin type "$funcname")

    local sed_expression="s/^${funcname} is a shell function from (.+)\$/\\1/g"
    full_path=$(echo "$builtin_type" | ggrep "is a shell function from" | sed -E "$sed_expression")

    if [[ -z "$full_path" ]]; then
      if ! echo "$builtin_type" | grep "is an alias for" > /dev/null; then
        echo "$builtin_type"
        return
      fi

      local grep_expression="^alias ${funcname} *="
      read -r full_path line_number <<< $(ggrep -nr "$grep_expression" "$search_dir" | head -n 1 | sed -E 's/^(.+):([0-9]+):.*$/\1 \2/g')
    else
      line_number=$(grep -n "${funcname}()" "$full_path" | sed -E 's/^([0-9]+):.*$/\1/g')
    fi

    if [[ -z "$full_path" ]]; then
      echo "$builtin_type"
      return
    fi

    nvim +"$line_number" +"normal! zz" "$full_path"
    return
  fi

  local grep_expression="^alias ${funcname} *="
  read -r full_path line_number <<< $(ggrep -nr "$grep_expression" "$search_dir" | head -n 1 | sed -E 's/^(.+):([0-9]+):.*$/\1 \2/g')

  if [[ -z "$full_path" ]]; then
    local grep_expression="^(${funcname}\(\)|function ${funcname}\(\))"
    read -r full_path line_number <<< $(ggrep -Enr "$grep_expression" "$search_dir" | head -n 1 | sed -E 's/^(.+):([0-9]+):.*$/\1 \2/g')
  fi

  if [[ -z "$full_path" ]]; then
    echo "$funcname not found."
    return
  fi

  nvim +"$line_number" +"normal! zz" "$full_path"
}

