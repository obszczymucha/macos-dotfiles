bindkey -M emacs '^[j' down-line-or-search
bindkey -M emacs '^[k' up-line-or-search
bindkey -M emacs '^[h' backward-char
bindkey -M emacs '^[l' forward-char
bindkey -M emacs '^[w' forward-word
bindkey -M emacs '^[0' beginning-of-line
bindkey -M emacs '^[4' end-of-line
bindkey -M emacs '^[u' undo
bindkey -M emacs '^[x' delete-char
bindkey -M emacs '^[c' kill-line
bindkey -M emacs '^[D' backward-kill-word
bindkey -M emacs '^[X' backward-delete-char

