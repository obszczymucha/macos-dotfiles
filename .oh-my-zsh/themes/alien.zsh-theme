# vim: set filetype=zsh :

function is_tmuxed() {
  if [[ -n $TMUX ]]; then
    echo "+ "
  else
    echo ""
  fi
}

PROMPT=' $(is_tmuxed)%F{white}%2c%F{cyan} [%f '
RPROMPT='$(git_prompt_info) %F{cyan}] %F{lightblue}%D{%H:%M}'

ZSH_THEME_GIT_PROMPT_PREFIX="%F{white}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%f"
ZSH_THEME_GIT_PROMPT_DIRTY=" %F{green}*%f"
ZSH_THEME_GIT_PROMPT_CLEAN=""
